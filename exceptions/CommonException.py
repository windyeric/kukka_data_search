
class CommonException(Exception):

    def __init__(self, code, message):
        self.__code = code
        self.__message = message
        super().__init__(message)

    @property
    def code(self):
        return self.__code

    @code.setter
    def code(self, code):
        self.__code = code

    @property
    def message(self):
        return self.__message

    @message.setter
    def message(self, message):
        self.__message = message
