from django.urls import path

from . import views

app_name = 'search'

urlpatterns = [
    path('', views.index, name='index'),
    path('days_buyer/', views.days_buyer, name='days_buyer'),
    path('class_buyer_and_newsletter/', views.class_buyer_and_newsletter, name='class_buyer_and_newsletter'),
    path('class_not_buyer_with_address/', views.class_not_buyer_with_address, name='class_not_buyer_with_address'),
    path('link_price_partial_cancel/', views.link_price_partial_cancel, name='link_price_partial_cancel'),
    path('first_buyer_count_month/', views.first_buyer_count_month, name='first_buyer_count_month'),
    path('re_buy_percentage/', views.re_buy_percentage, name='re_buy_percentage'),
    path('end_subscribe_count_monthly/', views.end_subscribe_count_monthly, name='end_subscribe_count_monthly'),
    path('subscribe_extension_count_monthly/', views.subscribe_extension_count_monthly, name='subscribe_extension_count_monthly'),
    path('join_user_count/', views.join_user_count, name='join_user_count'),
    path('join_month_next_month_buyer_count/', views.join_month_next_month_buyer_count, name='join_month_next_month_buyer_count'),
    path('offline_location_clients/', views.offline_location_clients, name='offline_location_clients'),
    path('buy_info_during/', views.buy_info_during, name='buy_info_during'),
    path('sell_product_info/', views.sell_product_info, name='sell_product_info'),
    path('delete_file/', views.delete_file, name='delete_file'),
    path('subscribers_cnt/', views.subscribers_cnt, name='subscribers_cnt'),
    path('invalid_point/', views.invalid_point, name='invalid_point'),
    path('join_user_by_monthly/', views.join_user_by_monthly, name='join_user_by_monthly'),
    path('payment_method_data/', views.payment_method_data, name='payment_method_data'),
    path('product_category_incoming_for_long/', views.product_category_incoming_for_long, name='product_category_incoming_for_long'),
    path('sell_info_about_product_item_code/', views.sell_info_about_product_item_code, name='sell_info_about_product_item_code'),
    path('sell_info_about_product_item_code_subscription/', views.sell_info_about_product_item_code_subscription, name='sell_info_about_product_item_code_subscription'),
    path('sell_info_about_product_item_code_regular_payment/', views.sell_info_about_product_item_code_regular_payment, name='sell_info_about_product_item_code_regular_payment'),
    # path('user/', views.search_user, name='user'),
    # path('buyer_count/', views.search_buyer_count_days, name='buyer_count'),
    # path('class_buyer_and_newsletter/', views.search_class_buyer_and_newsletter, name='class_buyer_and_newsletter')
]
