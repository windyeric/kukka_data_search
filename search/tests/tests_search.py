from django.test import TestCase
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status

SEARCH_URL = reverse('search:user')


class TestsSearch(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_searchUser(self):

        res = self.client.get(SEARCH_URL)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_buyer_count(self):
        params = {
            'start_day': '2020-01-01',
            'end_day': '2020-01-30',
        }
        res = self.client.get(reverse('search:buyer_count'), params)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_class_buyer_and_newsletter(self):
        res = self.client.get(reverse('search:class_buyer_and_newsletter'))
        self.assertEqual(res.status_code, status.HTTP_200_OK)
