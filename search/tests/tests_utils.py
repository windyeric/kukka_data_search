from django.test import TestCase

from search import utils


class TestUtil(TestCase):

    def test_date(self):
        day = utils.add_day('2020-03-31')
        print(day)
        self.assertEqual('2020-04-01', day)

        day2 = utils.add_month('2020-12-01')
        print(day2)


