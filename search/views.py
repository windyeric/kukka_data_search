from django.http import HttpResponse
from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.template import loader

from exceptions.CommonException import CommonException
from . import repository
from . import utils


def call_return(req, template, name, success, message=''):
    folder, files = utils.file_list_and_dir(name)
    context = {'success': success, 'message': message, 'folder': folder, 'files': files}
    return HttpResponse(template.render(context, req))


def call_return_with_data(req, template, name, success, data='', message=''):
    folder, files = utils.file_list_and_dir(name)
    context = {'success': success, 'message': message, 'data': data, 'folder': folder, 'files': files}
    return HttpResponse(template.render(context, req))


def index(req):
    """ home page"""

    template = loader.get_template('search/index.html')
    context = {}
    return HttpResponse(template.render(context, req))


def delete_file(req):
    """ 파일 삭제 """
    folder = req.GET.get('folder')
    file = req.GET.get('file')
    template = req.GET.get('template')
    utils.remove_file(folder+'/'+file)

    #print(template)
    return redirect(template)


def class_not_buyer_with_address(req):

    name = 'class_not_buyer_with_address'
    template = loader.get_template('search/'+name+'/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start = req.GET.get('start')
    address = req.GET.get('search_word')

    try:
        repository.search_not_buy_class_and_address(start, address)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)

    return redirect('search:'+name)


def sell_product_info(req):
    """판매 상품 조회"""
    name = 'sell_product_info'
    template = loader.get_template('search/'+name+'/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start = req.GET.get('start')
    p_name = req.GET.get('search_word')

    try:
        repository.sell_product_info(p_name, start)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)

    return redirect('search:'+name)


def class_buyer_and_newsletter(req):
    """클래스 구매고객 (뉴스레터 동의)"""

    name = 'class_buyer_and_newsletter'
    template = loader.get_template('search/'+name+'/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    try:
        repository.search_class_buyer_and_newsletter()
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def days_buyer(req):
    """기간내 구매자 조회 페이지 """

    name = 'days_buyer'
    template = loader.get_template('search/'+name+'/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start = req.GET.get('start')
    end = req.GET.get('end')

    try:
        repository.search_days_buyer(start, end)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def join_user_by_monthly(req):
    """회원가입자 수를 월별로 조회"""

    name = 'join_user_by_monthly'
    template = loader.get_template('search/'+name+'/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start = req.GET.get('start')
    end = req.GET.get('end')

    try:
        repository.join_user_by_monthly(start, end)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def link_price_partial_cancel(req):
    """ 링크 프라이스 부분취소 금액 확인"""

    name = 'link_price_partial_cancel'
    template = loader.get_template('search/'+name+'/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    order_code_list = req.GET.get('text_input')

    try:
        repository.link_price_partial_cancel(order_code_list)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def first_buyer_count_month(req):
    """카테고리 첫구매자수"""
    name = 'first_buyer_count_month'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')

    try:
        data = repository.product_category_list()
    except CommonException as a:
        return call_return(req, template, name, False, a.message)

    if search != '1':
        return call_return_with_data(req, template, name, True, data)

    product_category_list = req.GET.getlist('product_category')
    include = req.GET.get('is_include')

    is_member = -1 # req.GET.get('is_member')

    try:
        repository.first_buyer_count_month(product_category_list, include, is_member)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def product_category_incoming_for_long(req):
    """카테고리 첫구매자수"""
    name = 'product_category_incoming_for_long'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')

    try:
        data = repository.product_category_list()
    except CommonException as a:
        return call_return(req, template, name, False, a.message)

    if search != '1':
        return call_return_with_data(req, template, name, True, data)

    product_category_list = req.GET.getlist('product_category')
    include = req.GET.get('is_include')

    start = req.GET.get('start')
    end = req.GET.get('end')

    try:
        repository.product_category_incoming_for_long(product_category_list, include, start, end)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def re_buy_percentage(req):
    """재구매율"""

    name = 're_buy_percentage'
    template = loader.get_template('search/' + name + '/index.html')

    try:
        data = repository.product_category_list()
    except CommonException as a:
        return call_return(req, template, name, False, a.message)

    search = req.GET.get('search')

    if search != '1':
        return call_return_with_data(req, template, name, True, data)

    product_category_list = req.GET.getlist('product_category')

    include = req.GET.get('is_include')

    try:
        repository.re_buy_percentage(product_category_list, include)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def end_subscribe_count_monthly(req):
    """구독종료예정 수 조회"""

    name = 'end_subscribe_count_monthly'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start_date = req.GET.get('date')
    try:
        repository.end_subscribe_count_monthly(start_date)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def subscribe_extension_count_monthly(req):
    """구독종료예정 수 조회"""

    name = 'subscribe_extension_count_monthly'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start_date = req.GET.get('date')
    try:
        repository.subscribe_extension_count_monthly(start_date)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def join_user_count(req):
    """회원가입자수 조회"""

    name = 'join_user_count'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start = req.GET.get('start')
    end = req.GET.get('end')
    is_anony = 'TRUE' if len(req.GET.getlist('check_01')) > 0 else 'FALSE'
    is_non_registered = 'TRUE' if len(req.GET.getlist('check_02')) > 0 else 'FALSE'

    try:
        repository.join_user_count(start, end, is_anony, is_non_registered)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def join_month_next_month_buyer_count(req):
    """회원가입자수 조회"""

    name = 'join_month_next_month_buyer_count'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    date = req.GET.get('date')
    try:
        repository.join_month_next_month_buyer_count(date)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def offline_location_clients(req):
    """오프라인 특정지역 고객데이터 조회"""

    name = 'offline_location_clients'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    locations = req.GET.get('locations')
    not_include_location = req.GET.get('not_include_location')
    is_buyer_for_days = req.GET.get('is_buyer_for_days')
    start = req.GET.get('start')
    end = req.GET.get('end')
    try:
        repository.offline_location_clients(locations, not_include_location, is_buyer_for_days, start, end)
        return redirect('search:' + name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def buy_info_during(req):
    """특정기간동안 구매자의 정보를 조회(사용자아이디, 주문아이디, 주문번호, 전체구매횟수, 전화, 이메일, 뉴스레터, box_code, 새벽배송"""

    name = 'buy_info_during'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start = req.GET.get('start')
    end = req.GET.get('end')
    try:
        repository.buy_info_during(start, end)
        return redirect('search:' + name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def subscribers_cnt(req):
    name = 'subscribers_cnt'
    template = loader.get_template('search/' + name + '/index.html')
    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start_date = req.GET.get('date')

    try:
        repository.subscribers_cnt(start_date=start_date)
        return redirect('search:' + name)
    except CommonException as e:
        return call_return(req, template, name, False, e.message)


def invalid_point(req):
    name = 'invalid_point'
    template = loader.get_template('search/' + name + '/index.html')
    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    standard_date = req.GET.get('date')

    try:
        repository.invalid_point(standard_date=standard_date)
        return redirect('search:' + name)
    except CommonException as e:
        return call_return(req, template, name, False, e.message)


def payment_method_data(req):
    name = 'payment_method_data'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    start = req.GET.get('start')
    end = req.GET.get('end')
    try:
        repository.payment_method_data(start, end)
        return redirect('search:' + name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def sell_info_about_product_item_code(req):
    name = 'sell_info_about_product_item_code'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    order_code_list = req.GET.get('text_input')
    start = req.GET.get('start')
    end = req.GET.get('end')
    try:
        repository.sell_info_about_product_item_code(order_code_list, start, end)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def sell_info_about_product_item_code_subscription(req):
    name = 'sell_info_about_product_item_code_subscription'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    order_code_list = req.GET.get('text_input')
    start = req.GET.get('start')
    end = req.GET.get('end')
    try:
        repository.sell_info_about_product_item_code_subscription(order_code_list, start, end)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)


def sell_info_about_product_item_code_regular_payment(req):
    name = 'sell_info_about_product_item_code_regular_payment'
    template = loader.get_template('search/' + name + '/index.html')

    search = req.GET.get('search')
    if search != '1':
        return call_return(req, template, name, True)

    order_code_list = req.GET.get('text_input')
    start = req.GET.get('start')
    end = req.GET.get('end')
    try:
        repository.sell_info_about_product_item_code_regular_payment(order_code_list, start, end)
        return redirect('search:'+name)
    except CommonException as a:
        return call_return(req, template, name, False, a.message)
