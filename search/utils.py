import os
import datetime
import pandas as pd
from dateutil.relativedelta import relativedelta

from django.db import connection
from pandas.io.sql import DatabaseError

from exceptions.CommonException import CommonException

_media_dir = 'media'


class DateUtils:

    @staticmethod
    def year():
        t = datetime.datetime.now()
        return t.year

    @staticmethod
    def month():
        t = datetime.datetime.now()
        return t.month

    @staticmethod
    def day():
        t = datetime.datetime.now()
        return t.day

    @staticmethod
    def hour():
        t = datetime.datetime.now()
        return t.hour

    @staticmethod
    def min():
        t = datetime.datetime.now()
        return t.minute

    @staticmethod
    def sec():
        t = datetime.datetime.now()
        return t.second

    @staticmethod
    def add_sec(date, sec):
        return date + datetime.timedelta(0, sec)

    @staticmethod
    def yyyy_mm_dd_hhmmss(sep=''):
        t = datetime.datetime.now()
        return \
            str(t.year)+sep + \
            str(t.month)+sep + \
            str(t.day)+sep + \
            str(t.hour)+sep + \
            str(t.minute)+sep + \
            str(t.second)

    @staticmethod
    def now():
        return datetime.datetime.now()

    @staticmethod
    def str_to_datetime(sss):
        date = datetime.datetime.strptime(sss, '%Y-%m-%d %H:%M:%S.%f')
        return date


def remove_file(path):
    """파일 제거"""
    if os.path.isfile(path):
        os.remove(path)


def file_list_and_dir(dir):
    """디렉토리에 파일 리스트"""
    dir_name = _media_dir+"/"+dir

    try:
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        files = os.listdir(_media_dir + "/" + dir)
        files.sort(reverse=True)
    except OSError:
        raise CommonException("OSError", "파일 리스트 가져옴 " + dir_name)
    return dir_name, files


def query(sql):
    """쿼리 실행결과 리턴"""
    with connection.cursor() as cursor:
        cursor.execute(sql)
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]


def query_to_csv(sql, file_name):
    """쿼리 수행결과를 csv로 변환"""
    dir_name = _media_dir + "/" + file_name

    try:
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
    except OSError:
        raise CommonException("OSError", "디렉토리 생성에러 " + dir_name)

    #print(sql)

    file_name2 = dir_name + "/" + file_name + "_" + str(DateUtils.yyyy_mm_dd_hhmmss(sep='_')) + ".csv"

    f = open(
        file_name2,
        'w'
    )

    f.close()

    try:
        df = pd.read_sql_query(sql, connection)
        #print(df)
        df.to_csv(file_name2, encoding='utf-8-sig', index=False)
    except DatabaseError as e:

        remove_file(file_name2)
        raise CommonException("SQL Error", e)

    return file_name2


def add_day(date_str, add_days=1):
    """일수 증가 및 감소"""
    day = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    day = day + datetime.timedelta(days=add_days)
    return day.strftime('%Y-%m-%d')


def add_month(date_str, add_months=1):
    """달을 증가 및 감소하기"""
    day = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    day = day + relativedelta(months=add_months)
    return day.strftime('%Y-%m-%d')


def str_to_list(str, seq):
    """스트링을 구분자로 구분하여 dict로 반환"""
    result = str.split(seq)
    return result
