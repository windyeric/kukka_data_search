import datetime

from exceptions.CommonException import CommonException
from search import utils


def product_category_list():
    """프로덕트 카테고리 리스트 조회"""
    sql = "select id, biz_type, name, code from catalog_productcategory"
    return utils.query(sql)


def search_user():
    sql = "SELECT * FROM `kukkart`.account_kukkartuser WHERE id <10 "
    return utils.query_to_csv(sql, 'search_user')


def search_join_user_count(start, end):
    sql = f'SELECT COUNT(*) \n\
                FROM `kukkart`.account_kukkartuser ak  \n\
                WHERE date_add(ak.date_added, interval 9 hour) >= {start} \n\
                AND date_add(ak.date_added, interval 9 hour) < {utils.add_day(end)} \n\
                AND ak.is_anony = FALSE \n\
                AND ak.is_non_registered = FALSE;'
    return utils.query_to_csv(sql, 'search_join_user_count')


# 해당 달에 구매자수
def search_days_buyer(start, end):

    sql = f'select count(distinct oo.user_id)\n\
            from `kukkart`.order_order oo\n\
            join `kukkart`.account_kukkartuser ak on ak.id = oo.user_id\n\
            join `kukkart`.order_orderproduct op on op.order_id = oo.id\n\
            join `kukkart`.catalog_product cp on cp.id = op.product_id\n\
            where	\n\
            oo.status = 3\n\
            and cp.category_id not in (6, 20, 5)\n\
            and date_add(oo.date_added, interval 9 hour) >= "{start}"\n\
            and date_add(oo.date_added, interval 9 hour) < "{utils.add_day(end)}"\n\
            and ak.is_anony = false\n\
            '
    return utils.query_to_csv(sql, 'days_buyer')


# 클래스 구매 고객 (뉴스레터동의)
def search_class_buyer_and_newsletter():

    sql = f"SELECT          \n\
                ak.id,      \n\
                ak.name,    \n\
                ak.phone,   \n\
                ak.email    \n\
            FROM `kukkart`.account_kukkartuser ak \n\
                    JOIN `kukkart`.account_kukkartuseragreement a on ak.id = a.user_id   \n\
                    JOIN `kukkart`.order_order oo on ak.id = oo.user_id           \n\
                    JOIN `kukkart`.order_orderproduct o on oo.id = o.order_id     \n\
                    JOIN `kukkart`.catalog_product cp on o.product_id = cp.id     \n\
            WHERE \n\
                ak.email NOT LIKE '%kukka%' \n\
                AND ak.is_active=TRUE   \n\
                AND ak.is_anony = FALSE \n\
                AND ak.is_non_registered = FALSE  \n\
                AND a.newsletter = TRUE           \n\
                AND oo.status = 3                 \n\
                AND cp.category_id = 6            \n\
            GROUP BY  ak.id"

    return utils.query_to_csv(sql, 'class_buyer_and_newsletter')


def search_not_buy_class_and_address(start, address):

    sql = f"SELECT \n\
            ak.id,   \n\
            ak.name,   \n\
            ak.phone,    \n\
            ak.email     \n\
        FROM `kukkart`.account_kukkartuser ak \n\
            JOIN `kukkart`.account_kukkartuseragreement a on ak.id = a.user_id \n\
            JOIN `kukkart`.order_order oo on ak.id = oo.user_id            \n\
            JOIN `kukkart`.order_orderproduct o on oo.id = o.order_id      \n\
            JOIN `kukkart`.catalog_product cp on o.product_id = cp.id      \n\
        WHERE  \n\
            ak.email NOT LIKE '%kukka%' \n\
            AND ak.is_active=TRUE    \n\
            AND ak.is_anony = FALSE  \n\
            AND ak.is_non_registered = FALSE \n\
            AND a.newsletter = TRUE          \n\
            AND oo.status = 3                \n\
            AND cp.category_id != 6          \n\
            AND oo.id in ( \n\
                select user_id \n\
                from kukkart.order_orderaddress \n\
                Where  \n\
                    id in ( \n\
                            select orderaddress_id \n\
                            from kukkart.order_order_address \n\
                            where \n\
                                order_id in \n\
                                (select id from kukkart.order_order oo where date_add(oo.date_added, interval 9 hour) >= '{start}' order by oo.date_added) \n\
                    ) \n\
                    AND address_1 REGEXP '{address}') \n\
            GROUP BY  ak.id \n\
          "

    return utils.query_to_csv(sql, 'class_not_buyer_with_address')


def link_price_partial_cancel(order_code_list):

    sql = f"SELECT \n\
            ur.email '구매자',\n\
            oc.order_code '주문번호',\n\
            if( rf.amount is null , 'X' ,'O') as '부분취소여부',\n\
            oo.total_checkout as '최초결제금액',\n\
            rf.amount  as '취소금액', \n\
            rf.balance  as '취소후잔액', \n\
            rf.message as '사유'\n\
        FROM kukkart.order_ordercheckoutlinkprice oc\n\
        JOIN kukkart.order_order oo ON oo.no = oc.order_code\n\
        JOIN kukkart.account_kukkartuser ur on ur.id = oo.user_id\n\
        left JOIN order_partialrefund rf on rf.order_id=oo.id\n\
        where \n\
        order_code in ( {order_code_list})\n\
        order by order_code asc\n\
        "

    return utils.query_to_csv(sql, 'link_price_partial_cancel')


def first_buyer_count_month(category_list, include, is_member):
    """카테고리 첫구매자수"""

    c_list = ','.join(category_list)

    condition = ' in ' if include == '1' else ' not in '

    sql = f"SELECT \n\
                   DATE_FORMAT(t.order_ymd, '%Y-%m') order_ym,\n\
                   COUNT(*) total\n\
            FROM (\n\
                 SELECT MIN(DATE_ADD(oo.date_added, INTERVAL 9 HOUR)) order_ymd\n\
                  FROM order_orderproduct ob\n\
                      JOIN order_order oo on ob.order_id = oo.id\n\
                      JOIN account_kukkartuser ak on oo.user_id = ak.id\n\
                      JOIN catalog_product cp ON cp.id = ob.product_id\n\
                  WHERE\n\
                        oo.status = 3\n\
                        AND ak.is_anony = 0 \n\
                        AND ak.is_non_registered = 0\n\
                        AND cp.category_id  {condition} ({c_list})\n\
                  GROUP BY oo.user_id\n\
                 ) t\n\
            GROUP BY order_ym"

    return utils.query_to_csv(sql, 'first_buyer_count_month')


def re_buy_percentage(category_list, include):

    c_list = ','.join(category_list)

    condition = ' in ' if include == '1' else ' not in '

    sql = f"SELECT ob2.last_box_ym,\n\
                   SUM(ob2.afterone) after1,\n\
                   SUM(ob2.aftertwo) after2,\n\
                   SUM(ob2.afterthree) after3,\n\
                   SUM(ob2.afterfour) after4,\n\
                   SUM(ob2.afterfive) after5,\n\
                   SUM(ob2.aftersix) after6\n\
            FROM (\n\
                     SELECT\n\
                            DATE_FORMAT(ob1.order_ymd, '%Y-%m') last_box_ym,\n\
                            ob1.user_id,\n\
                            ob1.order_id,\n\
                            (SELECT COUNT(DISTINCT o1.user_id)\n\
                             FROM order_order o1\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_SUB(ob1.order_ymd, INTERVAL 9 HOUR)\n\
                                AND o1.date_added < DATE_ADD(ob1.order_ymd, INTERVAL 180 DAY)\n\
                            HAVING MIN(o1.date_added) < DATE_ADD(ob1.order_ymd, INTERVAL 30 DAY)\n\
                            ) afterone,\n\
                            (SELECT COUNT(DISTINCT o1.user_id)\n\
                             FROM order_order o1\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_SUB(ob1.order_ymd, INTERVAL 9 HOUR)\n\
                               AND o1.date_added < DATE_ADD(ob1.order_ymd, INTERVAL 180 DAY)\n\
                            HAVING\n\
                                   MIN(o1.date_added) >= DATE_ADD(ob1.order_ymd, INTERVAL 30 DAY)\n\
                               AND MIN(o1.date_added) < DATE_ADD(ob1.order_ymd, INTERVAL 60 DAY)\n\
                            ) aftertwo,\n\
                            (SELECT COUNT(DISTINCT o1.user_id)\n\
                             FROM order_order o1\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_SUB(ob1.order_ymd, INTERVAL 9 HOUR)\n\
                               AND o1.date_added < DATE_ADD(ob1.order_ymd, INTERVAL 180 DAY)\n\
                            HAVING\n\
                                   MIN(o1.date_added) >= DATE_ADD(ob1.order_ymd, INTERVAL 60 DAY)\n\
                               AND MIN(o1.date_added) < DATE_ADD(ob1.order_ymd, INTERVAL 90 DAY)\n\
                            ) afterthree,\n\
                            (SELECT COUNT(DISTINCT o1.user_id)\n\
                             FROM order_order o1\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_SUB(ob1.order_ymd, INTERVAL 9 HOUR)\n\
                               AND o1.date_added < DATE_ADD(ob1.order_ymd, INTERVAL 180 DAY)\n\
                            HAVING\n\
                                   MIN(o1.date_added) >= DATE_ADD(ob1.order_ymd, INTERVAL 90 DAY)\n\
                               AND MIN(o1.date_added) < DATE_ADD(ob1.order_ymd, INTERVAL 120 DAY)\n\
                            ) afterfour,\n\
                            (SELECT COUNT(DISTINCT o1.user_id)\n\
                             FROM order_order o1\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_SUB(ob1.order_ymd, INTERVAL 9 HOUR)\n\
                               AND o1.date_added < DATE_ADD(ob1.order_ymd, INTERVAL 180 DAY)\n\
                            HAVING\n\
                                   MIN(o1.date_added) >= DATE_ADD(ob1.order_ymd, INTERVAL 120 DAY)\n\
                               AND MIN(o1.date_added) < DATE_ADD(ob1.order_ymd, INTERVAL 150 DAY)\n\
                            ) afterfive,\n\
                            (SELECT COUNT(DISTINCT o1.user_id)\n\
                             FROM order_order o1\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_SUB(ob1.order_ymd, INTERVAL 9 HOUR)\n\
                               AND o1.date_added < DATE_ADD(ob1.order_ymd, INTERVAL 180 DAY)\n\
                            HAVING\n\
                                   MIN(o1.date_added) >= DATE_ADD(ob1.order_ymd, INTERVAL 150 DAY)\n\
                               AND MIN(o1.date_added) < DATE_ADD(ob1.order_ymd, INTERVAL 180 DAY)\n\
                            ) aftersix\n\
                     FROM (\n\
                         SELECT MIN(DATE_ADD(oo.date_added, INTERVAL 9 HOUR)) order_ymd,\n\
                                oo.user_id,\n\
                                MIN(oo.id) order_id\n\
                         FROM order_orderproduct ob\n\
                             JOIN order_order oo on ob.order_id = oo.id\n\
                             JOIN account_kukkartuser ak on oo.user_id = ak.id\n\
                             JOIN catalog_product cp ON cp.id = ob.product_id\n\
                         WHERE\n\
                               oo.status = 3\n\
                           AND ak.is_anony = 0\n\
                           AND ak.is_non_registered = 0\n\
                           AND cp.category_id  {condition} ({c_list})\n\
                         GROUP BY oo.user_id\n\
                         ) ob1\n\
                 ) ob2 \n\
            GROUP BY last_box_ym"

    return utils.query_to_csv(sql, 're_buy_percentage')


def end_subscribe_count_monthly(start_date):
    """구독종료예정 수 조회"""
    sql = f"SELECT\n\
               DATE_FORMAT(t.last_box, '%Y-%m') last_box_ym,\n\
               COUNT(*) total\n\
        FROM (\n\
            SELECT MAX(ob.box_date) last_box,\n\
                   o.user_id,\n\
                   o.id\n\
                   order_id,\n\
                   oo.product_id\n\
            FROM order_orderbox ob\n\
                JOIN order_orderproduct oo on ob.order_product_id = oo.id\n\
                JOIN order_order o on oo.order_id = o.id\n\
                JOIN account_kukkartuser ak on o.user_id = ak.id\n\
            WHERE o.status = 3\n\
              AND oo.box_type = 2\n\
              AND date_add(ob.box_date, interval 9 hour) >= '{start_date}'\n\
              AND oo.status = 2\n\
              AND ak.is_anony = FALSE\n\
              AND ak.is_non_registered = FALSE\n\
            GROUP BY ob.order_product_id\n\
            UNION ALL\n\
            SELECT MAX(ob.box_date) last_box,\n\
                   o.user_id,\n\
                   o.id   order_id,\n\
                   oo.product_id\n\
            FROM order_orderbox ob\n\
                JOIN order_orderproduct oo on ob.order_product_id = oo.id\n\
                JOIN order_order o on oo.order_id = o.id\n\
                JOIN account_kukkartuser ak on o.user_id = ak.id\n\
            WHERE o.status = 3\n\
              AND oo.box_type = 1\n\
              AND date_add(ob.box_date, interval 9 hour) >= '{start_date}'\n\
              AND ak.is_anony = FALSE\n\
              AND ak.is_non_registered = FALSE\n\
            GROUP BY ob.order_product_id\n\
            ) t JOIN catalog_product cp ON cp.id = t.product_id\n\
        WHERE cp.category_id = 1\n\
        GROUP BY last_box_ym\n\
        "

    return utils.query_to_csv(sql, 'end_subscribe_count_monthly')


def subscribe_extension_count_monthly(start_date):
    """특정시작일부터 구독 연장수 조회"""
    sql = f"SELECT ob2.last_box_ym, \n\
                   SUM(ob2.afterone) after1,\n\
                   SUM(ob2.aftertwo) after2,\n\
                   SUM(ob2.afterthree) after3,\n\
                   SUM(ob2.afterfour) after4,\n\
                   SUM(ob2.afterfive) after5,\n\
                   SUM(ob2.aftersix) after6\n\
            FROM (\n\
                     SELECT\n\
                            DATE_FORMAT(ob1.last_box, '%Y-%m') last_box_ym,\n\
                            (SELECT COUNT(o1.id)\n\
                             FROM order_order o1\n\
                                 JOIN order_orderproduct oo2 on o1.id = oo2.order_id\n\
                                 JOIN catalog_product c on oo2.product_id = c.id\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND c.category_id = 1\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_SUB(ob1.last_box, INTERVAL 9 HOUR)\n\
                               AND o1.date_added < DATE_ADD(ob1.last_box, INTERVAL 30 DAY)\n\
                            ) afterone,\n\
                            (SELECT COUNT(o1.id)\n\
                             FROM order_order o1\n\
                                 JOIN order_orderproduct oo2 on o1.id = oo2.order_id\n\
                                 JOIN catalog_product c on oo2.product_id = c.id\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND c.category_id = 1\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_ADD(DATE_SUB(ob1.last_box, INTERVAL 9 HOUR), INTERVAL 30 DAY)\n\
                               AND o1.date_added < DATE_ADD(ob1.last_box, INTERVAL 60 DAY)\n\
                            ) aftertwo,\n\
                            (SELECT COUNT(o1.id)\n\
                             FROM order_order o1\n\
                                 JOIN order_orderproduct oo2 on o1.id = oo2.order_id\n\
                                 JOIN catalog_product c on oo2.product_id = c.id\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND c.category_id = 1\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_ADD(DATE_SUB(ob1.last_box, INTERVAL 9 HOUR), INTERVAL 60 DAY)\n\
                               AND o1.date_added < DATE_ADD(ob1.last_box, INTERVAL 90 DAY)\n\
                            ) afterthree,\n\
                            (SELECT COUNT(o1.id)\n\
                             FROM order_order o1\n\
                                 JOIN order_orderproduct oo2 on o1.id = oo2.order_id\n\
                                 JOIN catalog_product c on oo2.product_id = c.id\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND c.category_id = 1\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_ADD(DATE_SUB(ob1.last_box, INTERVAL 9 HOUR), INTERVAL 90 DAY)\n\
                               AND o1.date_added < DATE_ADD(ob1.last_box, INTERVAL 120 DAY)\n\
                            ) afterfour,\n\
                            (SELECT COUNT(o1.id)\n\
                             FROM order_order o1\n\
                                 JOIN order_orderproduct oo2 on o1.id = oo2.order_id\n\
                                 JOIN catalog_product c on oo2.product_id = c.id\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND c.category_id = 1\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_ADD(DATE_SUB(ob1.last_box, INTERVAL 9 HOUR), INTERVAL 120 DAY)\n\
                               AND o1.date_added < DATE_ADD(ob1.last_box, INTERVAL 150 DAY)\n\
                            ) afterfive,\n\
                            (SELECT COUNT(o1.id)\n\
                             FROM order_order o1\n\
                                 JOIN order_orderproduct oo2 on o1.id = oo2.order_id\n\
                                 JOIN catalog_product c on oo2.product_id = c.id\n\
                             WHERE o1.user_id = ob1.user_id\n\
                               AND NOT o1.id = ob1.order_id\n\
                               AND c.category_id = 1\n\
                               AND o1.status = 3\n\
                               AND o1.date_added >= DATE_ADD(DATE_SUB(ob1.last_box, INTERVAL 9 HOUR), INTERVAL 150 DAY)\n\
                               AND o1.date_added < DATE_ADD(ob1.last_box, INTERVAL 180 DAY)\n\
                            ) aftersix\n\
                     FROM (\n\
                              SELECT MAX(ob.box_date) last_box,\n\
                                     o.user_id,\n\
                                     o.id             order_id,\n\
                                     oo.product_id\n\
                              FROM order_orderbox ob\n\
                                       JOIN order_orderproduct oo on ob.order_product_id = oo.id\n\
                                       JOIN order_order o on oo.order_id = o.id\n\
                                       JOIN account_kukkartuser ak on o.user_id = ak.id\n\
                              WHERE o.status = 3\n\
                                AND oo.box_type = 2\n\
                                AND ob.box_date >= '{start_date}'\n\
                                AND oo.status = 2\n\
                  AND ak.is_anony = FALSE\n\
                  AND ak.is_non_registered = FALSE\n\
                              GROUP BY ob.order_product_id\n\
                              UNION ALL\n\
                              SELECT MAX(ob.box_date) last_box,\n\
                                     o.user_id,\n\
                                     o.id             order_id,\n\
                                     oo.product_id\n\
                              FROM order_orderbox ob\n\
                                       JOIN order_orderproduct oo on ob.order_product_id = oo.id\n\
                                       JOIN order_order o on oo.order_id = o.id\n\
                                       JOIN account_kukkartuser ak on o.user_id = ak.id\n\
                              WHERE o.status = 3\n\
                                AND oo.box_type = 1\n\
                                AND ob.box_date >= '{start_date}'\n\
                  AND ak.is_anony = FALSE\n\
                  AND ak.is_non_registered = FALSE\n\
                              GROUP BY ob.order_product_id\n\
                          ) ob1\n\
                         JOIN catalog_product cp ON cp.id = ob1.product_id\n\
                     WHERE cp.category_id = 1\n\
                 ) ob2\n\
            GROUP BY last_box_ym\n\
            "

    return utils.query_to_csv(sql, 'subscribe_extension_count_monthly')


def join_user_count(start, end, is_anony, is_non_registered):
    """회원가입자 수"""
    sql = f"SELECT COUNT(*) \n\
            FROM account_kukkartuser ak \n\
            WHERE date_add(ak.date_added, interval 9 hour) >= '{start}' \n\
            AND date_add(ak.date_added, interval 9 hour) < '{utils.add_day(end)}' \n\
            AND ak.is_anony = {is_anony}  \n\
            AND ak.is_non_registered = {is_non_registered} \n\
            "

    return utils.query_to_csv(sql, 'join_user_count')


def join_month_next_month_buyer_count(date):
    """회원가입당일 다음월 구매자수"""

    sql = f"\
            SELECT\n\
                COUNT(CASE WHEN a.p > 0 THEN a.email END) AS '01',\n\
                COUNT(CASE WHEN a.q > 0 THEN a.email END) AS '02'\n\
            FROM(\n\
                SELECT ku.email,\n\
                       COUNT(\n\
                           CASE\n\
                               WHEN date_add(oo.date_added, interval 9 hour) >= '{date}'\n\
                               AND date_add(oo.date_added, interval 9 hour) < '{utils.add_month(date, add_months=1)}'\n\
                               THEN ku.email\n\
                               END\n\
                           ) AS 'p',\n\
                       COUNT(\n\
                           CASE\n\
                               WHEN date_add(oo.date_added, interval 9 hour) >= '{utils.add_month(date, add_months=1)}'\n\
                               AND date_add(oo.date_added, interval 9 hour) < '{utils.add_month(date, add_months=2)}'\n\
                               THEN ku.email\n\
                               END\n\
                           ) AS 'q'\n\
                FROM order_order AS oo\n\
                    JOIN account_kukkartuser AS ku\n\
                    ON ku.id = oo.user_id\n\
                WHERE oo.status = 3\n\
                    AND oo.total_checkout > 1000\n\
                    AND date_add(ku.date_added, interval 9 hour) >= '{date}'\n\
                    AND date_add(ku.date_added, interval 9 hour) < '{utils.add_month(date, add_months=1)}'\n\
                    AND ku.is_anony = FALSE\n\
                    AND ku.is_non_registered = FALSE\n\
                GROUP BY ku.email\n\
            ) AS a\n\
            "
    return utils.query_to_csv(sql, 'join_month_next_month_buyer_count')


def offline_location_clients(locations, not_include_location, is_buyer_for_days, start, end):
    """오프라인 특정지역 고객 데이터 조회"""
    day_sql = ''
    if not is_buyer_for_days is None:
        day_sql = f"oo.date_added  between  '{start}' AND '{end}'  AND "

    exclude_location_str = ''
    if not_include_location is None or not_include_location.strip() == '':
        exclude_location_str = ''
    else:
        exclude_location_list = utils.str_to_list(not_include_location, "|")
        for d in exclude_location_list:
            exclude_location_str += f"AND uad.address_1  NOT LIKE '%{d}%' "

    sql = f"SELECT \n\
                u2.name, \n\
                u2.phone, \n\
                if(u2.gender=1,'여','남') as gender,\n\
                (SELECT type_name FROM kukkart.account_kukkartuserleveltype ult WHERE ult.id = ul.type_id) as level,\n\
                concat(uad2.address_1, uad2.address_2) as address,\n\
                u2.email \n\
            FROM \n\
                kukkart.account_kukkartuser u2\n\
                JOIN kukkart.account_kukkartuserlevel ul ON  ul.user_id = u2.id\n\
                JOIN kukkart.order_orderaddress uad2 ON u2.id = uad2.user_id\n\
            WHERE uad2.id in (\n\
            SELECT \n\
                    max(uad.id)\n\
                FROM \n\
                    kukkart.account_kukkartuser u\n\
                    JOIN kukkart.order_orderaddress uad ON u.id = uad.user_id\n\
                    JOIN kukkart.account_kukkartuseragreement ua ON u.id = ua.user_id AND ua.newsletter = 1\n\
                    JOIN kukkart.order_order oo ON  {day_sql} \n\
                                oo.user_id=u.id \n\
            WHERE \n\
                    u.is_active = TRUE\n\
                    AND u.is_anony = FALSE\n\
                    AND u.is_non_registered = FALSE\n\
                    AND u.email not like '%@kukkainactive.kr'\n\
                    AND uad.address_1 REGEXP '{locations}' \n\
                    {exclude_location_str} \n\
            group by u.phone\n\
        ) \n\
        order by u2.phone"

    return utils.query_to_csv(sql, 'offline_location_clients')


def buy_info_during(start, end):
    sql = f"SELECT \n\
                u.id as user_id, \n\
                oo.id as order_id,\n\
                oo.no as order_no,\n\
                (select count(*) from kukkart.order_order where user_id=u.id) as 전체구매횟수,\n\
                (select phone from kukkart.account_kukkartuser where id=u.id) as phone,\n\
                (select email from kukkart.account_kukkartuser where id=u.id) as email,\n\
                ag.newsletter as 뉴스레터수신여부,\n\
                (\n\
                SELECT\n\
                        group_concat( _ic.value separator '+')\n\
                    FROM \n\
                        kukkart.catalog_itemcode _ic\n\
                    WHERE             \n\
                        _ic.id in \n\
                            (\n\
                                SELECT _obi.item_code_id code\n\
                                FROM kukkart.order_orderboxitem _obi\n\
                                WHERE \n\
                                    _obi.order_box_id IN (\n\
                                                SELECT _ob.id \n\
                                                FROM kukkart.order_orderbox _ob\n\
                                                            JOIN kukkart.order_orderboxitem _obi ON  _obi.order_box_id = _ob.id\n\
                                                WHERE _ob.order_id=ob.order_id\n\
                                            )\n\
                            )   \n\
                )as box_code,\n\
                (SELECT if( count(*)>0, 1, 0)  FROM kukkart.order_ordermidnightinfo WHERE order_id = oo.id) as 새벽\n\
                FROM kukkart.order_orderbox ob\n\
                JOIN kukkart.order_order oo ON oo.id = ob.order_id\n\
                JOIN kukkart.account_kukkartuser u ON u.id = oo.user_id\n\
                JOIN kukkart.account_kukkartuseragreement ag ON ag.user_id=u.id AND ag.newsletter=1\n\
                WHERE\n\
                        date_add(ob.date_added, interval 9 hour) >= '{start}' AND date_add(ob.date_added, interval 9 hour) < '{utils.add_day(end, 1)}' \n\
                AND ob.status in (4,6)\n\
                ORDER BY u.id asc"

    return utils.query_to_csv(sql, 'buy_info_during')


def sell_product_info(name, start_date):

    sql = f"SELECT \n\
                oo.user_id, \n\
                oo.no  as order_no,\n\
                op.name as name ,  \n\
                (\n\
                        SELECT\n\
                            group_concat( _ic.value separator '+')\n\
                        FROM \n\
                            kukkart.catalog_itemcode _ic\n\
                        WHERE             \n\
                            _ic.id in \n\
                                (\n\
                                    SELECT _obi.item_code_id code\n\
                                    FROM kukkart.order_orderboxitem _obi\n\
                                    WHERE \n\
                                        _obi.order_box_id IN (\n\
                                                    SELECT _ob.id \n\
                                                    FROM kukkart.order_orderbox _ob\n\
                                                                JOIN kukkart.order_orderboxitem _obi ON _obi.code LIKE '%{name}%' AND _obi.order_box_id = _ob.id\n\
                                                    WHERE _ob.order_id=ob.order_id\n\
                                                )\n\
                                )         \n\
                 )  as box_code, \n\
                 (\n\
                    SELECT\n\
                        group_concat(_obi.code)\n\
                    FROM\n\
                        kukkart.order_orderboxitem _obi\n\
                    WHERE \n\
                        _obi.order_box_id IN (SELECT id FROM kukkart.order_orderbox WHERE order_id=oo.id)\n\
                        AND _obi.item_type = 'option'\n\
                 ) as box_option,\n\
                 (SELECT id FROM kukkart.catalog_itemcode WHERE value = op.code) as category_id,\n\
                 (SELECT name FROM kukkart.catalog_itemcode WHERE value = op.code) as category_name,\n\
                 oo.delivery_price+oo.total as 총금액,\n\
                 oo.total_checkout as 결제금액\n\
            FROM \n\
                kukkart.order_orderbox ob\n\
                INNER JOIN kukkart.order_order oo ON oo.id = ob.order_id\n\
                INNER JOIN kukkart.order_orderboxitem obi ON ob.id=obi.order_box_id\n\
                INNER JOIN kukkart.catalog_itemcode ic ON obi.item_code_id = ic.id\n\
                INNER JOIN kukkart.order_orderproduct op ON op.order_id=ob.order_id\n\
            WHERE\n\
                ob.box_date >=  DATE_SUB('{start_date}', INTERVAL 9 HOUR)\n\
                AND ic.value like '%{name}%' \n\
                AND ob.status = 4\n\
            ORDER BY oo.no ASC"

    return utils.query_to_csv(sql, 'sell_product_info')


def subscribers_cnt(start_date):

    # op.box_type = 1 이면서 op.box_quantity > 1 인 경우 정기구독
    # op.box_type = 2 인 경우 정기결제
    query = f"(SELECT op.name, op.code, op.box_option_name,\n\
                CASE\n\
                    WHEN ANY_VALUE(op.box_type) = 1 THEN '정기구독'\n\
                    WHEN ANY_VALUE(op.box_type) = 2 THEN '정기결제'\n\
                END AS box_type,\n\
                COUNT(op.id) as cnt FROM (\n\
            SELECT op.id\n\
                FROM order_orderbox AS ob\n\
                LEFT JOIN order_order AS oo ON ob.order_id = oo.id\n\
                LEFT JOIN order_orderproduct op ON oo.id = op.order_id\n\
                LEFT JOIN catalog_product cp ON op.product_id = cp.id\n\
                LEFT JOIN account_kukkartuser ak ON oo.user_id = ak.id\n\
                WHERE ob.box_date >= '{start_date}'\n\
                    AND oo.status = 3\n\
                    AND ob.status in (2, 3, 4, 5)\n\
                    AND op.status = 1\n\
                    AND op.box_type = 2\n\
                    AND cp.category_id = 1\n\
                    AND ak.is_anony = false\n\
                    AND ak.is_non_registered = false\n\
                GROUP BY op.id) AS a\n\
            LEFT JOIN order_orderproduct AS op ON a.id = op.id\n\
        GROUP BY op.name, op.code, op.box_option_name)\n\
        UNION\n\
        (SELECT op.name, op.code, op.box_option_name,\n\
                CASE\n\
                    WHEN ANY_VALUE(op.box_type) = 1 THEN '정기구독'\n\
                    WHEN ANY_VALUE(op.box_type) = 2 THEN '정기결제'\n\
                END AS box_type,\n\
                COUNT(op.id) AS cnt from (\n\
            SELECT op.id\n\
                FROM order_orderproduct op\n\
                    LEFT JOIN order_order AS oo ON op.order_id = oo.id\n\
                    LEFT JOIN order_orderbox AS ob ON ob.order_id = oo.id\n\
                    LEFT JOIN catalog_product cp ON op.product_id = cp.id\n\
                    LEFT JOIN account_kukkartuser ak ON oo.user_id = ak.id\n\
                WHERE date_add(ob.box_date, interval 9 hour) >= '{start_date}'\n\
                    AND oo.status = 3\n\
                    AND ob.status in (2, 3, 4, 5)\n\
                    AND op.status = 1\n\
                    AND op.box_type = 1\n\
                    AND op.box_quantity > 1\n\
                    AND cp.category_id = 1\n\
                    AND ak.is_anony = false\n\
                    AND ak.is_non_registered = false\n\
                GROUP BY op.id) AS a\n\
            LEFT JOIN order_orderproduct AS op ON a.id = op.id\n\
        GROUP BY op.name, op.code, op.box_option_name);"

    return utils.query_to_csv(query, 'subscribers_cnt')


def invalid_point(standard_date):
    """기준일로 포인트 삭제 정보 조회"""

    sql = f"SELECT re.*, (re.point_for_before + re.point_for_after + re.user_point) as rest,  \n\
            if(re.point_for_before < (-1*re.user_point), 0, (re.point_for_before + re.user_point)) as remove_point \n\
            FROM \n\
            (\n\
                SELECT  \n\
                    b.user_id, \n\
                    c.email,\n\
                    c.phone,\n\
                    b.id as point_id,\n\
                    ( select ifnull(sum(a.value),0) from kukkart.voucher_pointhistory a where date_add(a.date_added, interval 9 hour) <= '{standard_date}' and a.value>0 and a.point_id=b.id  ) as point_for_before, \n\
                    ( select ifnull(sum(a.value),0) from kukkart.voucher_pointhistory a where date_add(a.date_added, interval 9 hour) > '{standard_date}' and a.value>0 and a.point_id=b.id  )  as point_for_after, \n\
                    ( select ifnull(sum(a.value),0) from kukkart.voucher_pointhistory a where a.value<0 and a.point_id=b.id) as user_point \n\
                FROM kukkart.voucher_point b \n\
                    JOIN kukkart.account_kukkartuser c ON c.id=b.user_id \n\
                ) as re \n\
                WHERE NOT(re.point_for_before =0 AND re.point_for_after = 0 AND re.user_point = 0)"

    return utils.query_to_csv(sql, 'invalid_point')


def join_user_by_monthly(start, end):
    """회원가입자수를 월별로 조회"""
    sql =f"SELECT \n\
    re.d as 날짜, \n\
    (SELECT \n\
            COUNT(*)\n\
        FROM\n\
            account_kukkartuser\n\
        WHERE\n\
            re.d = DATE_FORMAT(date_added, '%Y-%m-%d')) AS total,\n\
    (SELECT\n\
            COUNT(*)\n\
        FROM\n\
            account_kukkartuser\n\
        WHERE\n\
            re.d = DATE_FORMAT(date_added, '%Y-%m-%d')\n\
                AND is_anony = TRUE) AS 외부채널b2b,\n\
    (SELECT\n\
            COUNT(*)\n\
        FROM\n\
            account_kukkartuser\n\
        WHERE\n\
            re.d = DATE_FORMAT(date_added, '%Y-%m-%d')\n\
                AND is_non_registered = TRUE) AS 비회원구매,\n\
    (SELECT\n\
            COUNT(*)\n\
        FROM\n\
            account_kukkartuser\n\
        WHERE\n\
            re.d = DATE_FORMAT(date_added, '%Y-%m-%d')\n\
                AND is_non_registered = FALSE\n\
                AND is_anony = FALSE) AS 순수회원가입\n\
        FROM\n\
            (SELECT\n\
                 DATE_FORMAT(date_add(date_added, interval 9 hour) , '%Y-%m-%d') AS d\n\
            FROM\n\
                account_kukkartuser\n\
            WHERE\n\
                date_add(date_added, interval 9 hour) >= DATE_FORMAT('{start}', '%Y-%m-%d')\n\
                    AND date_add(date_added, interval 9 hour) < DATE_FORMAT( '{utils.add_day(end)}', '%Y-%m-%d')\n\
            GROUP BY d) re"

    return utils.query_to_csv(sql, 'join_user_by_monthly')


def product_category_incoming_for_long(category_list, include, start, end):

    c_list = ','.join(category_list)

    condition = ' in ' if include=='1' else ' not in '

    sql = f"SELECT \n\
            DATE_FORMAT(DATE_ADD(oo.date_added, INTERVAL 9 HOUR),\n\
                    '%Y-%m') AS dd,\n\
            SUM(oo.total_checkout) AS 매\n\
        FROM\n\
            order_order oo\n\
        WHERE\n\
            oo.id IN (SELECT \n\
                     op.order_id\n\
                FROM\n\
                    kukkart.order_orderproduct op\n\
                    JOIN \n\
                        kukkart.catalog_product cp \n\
                            ON cp.id = op.product_id AND\n\
                            cp.category_id {condition} ({c_list})  \n\
                            )\n\
               AND date_add(oo.date_added, interval 9 hour) >= '{start}' \n\
               AND date_add(oo.date_added, interval 9 hour) < '{utils.add_day(end)}'\n\
        GROUP BY dd "

    return utils.query_to_csv(sql, 'product_category_incoming_for_long')


def payment_method_data(start, end):

    SQL = f"SELECT  \n\
            CASE WHEN pay.method=1 THEN '신용카드' \n\
                     WHEN pay.method=2 THEN '신용카드(직접입력)' \n\
                     WHEN pay.method=3 THEN '휴대폰결제' \n\
                     WHEN pay.method=4 THEN '무통장입금'  \n\
                     WHEN pay.method=5 THEN '오프라인결제' \n\
                     WHEN pay.method=6 THEN 'PAYPAL' \n\
                     WHEN pay.method=7 THEN 'FANPAY 정기결제' \n\
                     WHEN pay.method=8 THEN 'FANPAY 일반결제' \n\
                     WHEN pay.method=9 THEN '스마일페이' \n\
                     WHEN pay.method=10 THEN '이벤트용 신용카드' \n\
                     WHEN pay.method=11 THEN '카카오페이' \n\
                     WHEN pay.method=12 THEN 'PAYCO'  \n\
                     ELSE 'NONE' END AS 결제방법,\n\
            sum(pay.amount) AS 매출액 ,\n\
            count(pay.amount) AS 결제횟수 \n\
        from order_orderpayment pay \n\
        where pay.date_added >= '{start}' and pay.date_added <= '{end}' group by pay.method \n\
        union \n\
        SELECT \n\
            CASE WHEN pay.method=1 THEN '신용카드' \n\
                     WHEN pay.method=2 THEN '신용카드(직접입력)' \n\
                     WHEN pay.method=3 THEN '휴대폰결제' \n\
                     WHEN pay.method=4 THEN '무통장입금'  \n\
                     WHEN pay.method=5 THEN '오프라인결제' \n\
                     WHEN pay.method=6 THEN 'PAYPAL' \n\
                     WHEN pay.method=7 THEN 'FANPAY 정기결제' \n\
                     WHEN pay.method=8 THEN 'FANPAY 일반결제' \n\
                     WHEN pay.method=9 THEN '스마일페이' \n\
                     WHEN pay.method=10 THEN '이벤트용 신용카드' \n\
                     WHEN pay.method=11 THEN '카카오페이' \n\
                     WHEN pay.method=12 THEN 'PAYCO'  \n\
                     ELSE 'NONE' END AS 결제방법,\n\
             sum(pay.amount) AS 매출액 ,\n\
            count(pay.amount) AS 결제횟수 \n\
        from order_orderpaymentrecurring pay\n\
        where pay.date_added >= '{start}' and pay.date_added <= '{end}' group by pay.method"

    return utils.query_to_csv(SQL, 'payment_method_data')


def sell_info_about_product_item_code(code_list,start,end):

    SQL = f" \n\
            select \n\
                item.value, \n\
                op.box_quantity as 박스개수, \n\
                count(oo.id) as 판매수량, \n\
                sum(oo.total) as 판매금액, \n\
                sum(oo.delivery_price) as 배송비,     \n\
                sum((select sum(amount) from order_orderpayment  where order_id=oo.id)) as 최종지불금 \n\
            from  \n\
                order_order oo \n\
                join order_orderproduct op \n\
                    on op.order_id = oo.id \n\
                        AND op.box_type=1 \n\
                JOIN catalog_itemcode item ON item.id = op.item_code_id \n\
            where \n\
                oo.status=3 \n\
                AND date_format(oo.date_added, '%Y-%m-%d') >= '{start}'  AND  date_format(oo.date_added, '%Y-%m-%d')  <= '{end}' \n\
            group by item.value, op.box_quantity  \n\
            having item.value in ( \n\
                    {code_list} \n\
             )"

    return utils.query_to_csv(SQL, 'sell_info_about_product_item_code')


def sell_info_about_product_item_code_subscription(code_list,start,end):

    SQL = f"\n\
            select \n\
                item.value, \n\
                op.box_quantity as 박스개수, \n\
                count(oo.id) as 판매수량, \n\
                sum(oo.total) as 판매금액, \n\
                sum(oo.delivery_price) as 배송비,     \n\
                sum((select sum(amount) from order_orderpayment  where order_id=oo.id)) as 최종지불금 \n\
            from  \n\
                order_order oo \n\
                join order_orderproduct op \n\
                    on op.order_id = oo.id \n\
                        AND op.product_id in(select id from kukkart.catalog_product where category_id in (1,5)) \n\
                        AND op.box_type=1 \n\
                JOIN catalog_itemcode item ON item.id = op.item_code_id \n\
            where \n\
                oo.status=3 \n\
                AND date_format(oo.date_added, '%Y-%m-%d') >= '{start}' AND  date_format(oo.date_added, '%Y-%m-%d')  <= '{end}' \n\
            group by item.value, op.box_quantity  \n\
            having item.value in ( {code_list} )"

    return utils.query_to_csv(SQL, 'sell_info_about_product_item_code_subscription')


def sell_info_about_product_item_code_regular_payment(code_list, start, end):
    ''' 정기결재 '''

    SQL = f"\n\
            select \n\
                item.value, \n\
                op.box_quantity as 박스개수, \n\
                count(oo.id) as 판매수량, \n\
                sum(oo.total) as 판매금액, \n\
                sum(oo.delivery_price) as 배송비,     \n\
                sum((select sum(amount) from order_orderpaymentrecurring  where order_id=oo.id)) as 최종지불금 \n\
            from  \n\
                order_order oo \n\
                join order_orderproduct op \n\
                    on op.order_id = oo.id \n\
                        AND op.product_id in(select id from kukkart.catalog_product where category_id in (1,5)) \n\
                        AND op.box_type=2 \n\
                JOIN catalog_itemcode item ON item.id = op.item_code_id \n\
            where \n\
                oo.status=3 \n\
                AND date_format(oo.date_added, '%Y-%m-%d') >= '{start}' AND date_format(oo.date_added, '%Y-%m-%d')  <= '{end}' \n\
            group by item.value, op.box_quantity  \n\
            having item.value in ( {code_list} )"

    return utils.query_to_csv(SQL, 'sell_info_about_product_item_code_regular_payment')

