# kukka Data Search (Prototype project)

### http://13.209.189.181:8000/search/

### 1. 개발환경
    - framework: Django
    - DB: kukka back DB(1일 1백업 DB를 기반으로 조회처리) 
    - source: https://bitbucket.org/windyeric/kukka_data_search/src/developer/
        - branch: developer
    - Server
        - AWS: kukka-2-db-rep
            - IP: 13.209.189.181
            - username: ubuntu
            - key: kukka-sub.pem
        - 위치: /home/ubuntu/statistics

### 2. 시작
    - project folder로 이동
    - pipenv shell
    - gunicorn --bind=0.0.0.0:8000 config.wsgi:application &

## 3. 조회등록
    - search/repository.py에 함수 하나 생성 - 생성이름중요함
        - sql을 생성하고 utils.query_to_csv(sql, '함수이름') -> sql조회 결과를 csv파일로 저장
    - search/views.py에 '함수이름'으로 함수하나 생성
        - parameter 넘겨받고 (다른 views의 함수 참조)
        - repository 호출
    - search/urls.py에 views에 생성한 함수를 연결
    - search/templates/search/index.html 에 방금생성한 조회 항목을 추가
    - search/templates/search/함수이름/index.html 을 생성하여 해당 조회를 위한 화면개발
        - 화면개발시 공통처리된 다른 index.html을 참조
        - 새롭게 나온 조회화면이라면 공통처리 필요함
        - 공통처리 templates는 프로젝트 root의 /templates폴더 참조

## 4. 참고
    - 현재 utilities는 search/utils.py 1개로만 운영중 - 향후 리팩토링필요
    - front는 부트스트랩 libs를 사용중

## 5. 사용법
    - 해당 조회조건을 넣고 조회
    - loading이 사라질때까지 기다림
    - csv파일 생성된것을 확인(파일명에 날짜,시간확인가능)
    - 필요 없는 파일들은 delete링크를 눌러서 삭제
